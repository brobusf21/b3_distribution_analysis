/* Author: Brandon
 * Description: Used for setting up Express application. 
 */

/* Load the module dependencies */
var express = require('./config/express');

/* Create a new Express application instance */
var app = express();

app.listen(3000);

console.log('Server is running at http://localhost:3000/);

/* Expose our application instance */
module.exports = app;
